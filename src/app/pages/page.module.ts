import { NgModule } from "@angular/core";
import { PAGES_ROUTES } from "./pages.routes";

import { FormsModule } from "@angular/forms";

import { DashboardComponent } from "./../pages/dashboard/dashboard.component";
import { ProgressComponent } from "./../pages/progress/progress.component";
import { Graficas1Component } from "./../pages/graficas1/graficas1.component";
import { PagesComponent } from "./pages.component";
import { SharedModule } from "../shared/shared.module";
import { IncrementadorComponent } from "../components/incrementador/incrementador.component";

@NgModule({
  declarations: [
    DashboardComponent,
    ProgressComponent,
    Graficas1Component,
    PagesComponent,
    IncrementadorComponent
  ],
  exports: [
    DashboardComponent,
    ProgressComponent,
    Graficas1Component,
    PagesComponent
  ],
  imports: [SharedModule, PAGES_ROUTES, FormsModule]
})
export class PagesModule {}
